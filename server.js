
var express = require('express'),
  app = express(),
  port = 3001,
  mongoose = require('mongoose'),
  EcommerceMapping = require('./api/models/ecommerceMappingModel'), //created model loading here
  bodyParser = require('body-parser'),
  Apify = require('apify');  ;


// mongoose instance connection url connection
mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost:27020/crawlerDB'); 


app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());


var routes = require('./api/routes/ecommerceMappingRoute'); //importing route
routes(app); //register the route


app.listen(port);


console.log('Arturo Velázquez Vargas Rest API Server port: ' + port);