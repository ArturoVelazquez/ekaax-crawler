
module.exports = function(app) {
  var crawlerController = require('../controllers/ecommerceMappingController');

  // todoList Routes
  app.route('/ecommerceMappings')
    .get(crawlerController.getAll)
    .post(crawlerController.create);


  app.route('/ecommerceMappings/:mappingId')
    .get(crawlerController.getById)
    .put(crawlerController.update)
    .delete(crawlerController.delete);

  app.route('/ecommerceMappings/scrap')
  .post(crawlerController.scrap);
};
