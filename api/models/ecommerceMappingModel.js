'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var EcommerceMappingSchema = new Schema({
  id: {
    type: String
  },
  ecommerceId: {
    type: String
  },
  url: {
    type: String 
  },
  status: {
    type: Boolean 
  },
  categoryId: {
    type: String 
  },
  type: {
    type: String 
  },
  parameters:[ {
    key:{
      type:String
    },
    value:{
      type:String
    },
    type:{
      type:Number
    }

  }]
});

module.exports = mongoose.model('EcommerceMapping', EcommerceMappingSchema);
