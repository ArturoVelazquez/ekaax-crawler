

var mongoose = require('mongoose'),
  EcommerceMapping = mongoose.model('EcommerceMapping');
const Apify = require('apify'); 


const EcommerceURLAssignment=require('../models/ecommerceURLAssignment.js');
const Ecommerce=require('../models/ecommerce.js');
const ProductMapped=require('../models/productMapped.js');



exports.getAll = function(req, res) {
  EcommerceMapping.find({}, function(err, task) {
    if (err)
      res.send(err);
    res.json(task);
  });
};




exports.create = function(req, res) {
  var ecommerceMapping = new EcommerceMapping(req.body);
  ecommerceMapping.save(function(err, task) {
    if (err)
      res.send(err);
    res.json(task);
  });
};


 exports.scrap = async (req, res) => {

    let productsResult = [];

    const ecommerceMapping = new EcommerceMapping(req.body);
/**
    EcommerceMapping.find({}, function(err, response) {
        if (err) {
            res.send(error);
        }
        console.log(response);
    });
*/
    var crawler = new Promise(
        // La función resolvedora es llamada con la
        // habilidad de resolver o rechazar la promesa
        async (resolve, reject) => {

            try {
                const ecommerce = {};
                const requestQueue = await Apify.openRequestQueue();

                await requestQueue.addRequest({
                    url: ecommerceMapping.url
                });
                // const pseudoUrls = [new Apify.PseudoUrl('https://super.walmart.com.mx/botanas-y-fruta-seca/rancheritos-rancheritos-familiar-240-g/[.*]')];

                let productsResult = [];

                const crawler = new Apify.PuppeteerCrawler({
                    requestQueue,
                    handlePageFunction: async ({
                        request,
                        page
                    }) => {
                        await Apify.utils.puppeteer.injectJQuery(page);
                
                        const productToBeMapped = {};

                        const productId = await page.evaluate(() => {
                            return $("body #root div div main div section div div div.vQMIKtScxt-0gP4BIwEhI span._3xnlUpxXqzvfm_fRoRHUf6 p._34YSfGmQlYtq5kY7JLZ7Ib._2Q77lQio2oYR8Hw1RSqLcu").text();
                        });

                        if (productId != null && productId != undefined) {
                            productToBeMapped.productId = productId;
                            const name = await page.evaluate(() => {
                                return $("body #root div div main div section div div div.vQMIKtScxt-0gP4BIwEhI span._3xnlUpxXqzvfm_fRoRHUf6 h1._36h98f465_cFppJAL38i7q.header").text();
                            });
                            productToBeMapped.name = name;

                            productsResult.push(productToBeMapped);
                        }

                        await Apify.utils.sleep(1 * 1000);
                    },
                    maxRequestsPerCrawl: 100,
                    maxConcurrency: 10,
                });


                await crawler.run();
                await requestQueue.delete();

                resolve(productsResult);

            } catch (error) {
              console.log(error);
                let errorObject = {
                    "errorMessage": error.toString()
                };
                reject(errorObject);

            }

        }
    );
    crawler.then(
            // Registrar el valor de la promesa cumplida
            function(val) {
                res.json(val);
            })
        .catch(
            // Registrar la razón del rechazo
            function(reason) {
                res.send(reason)
            });


};



exports.getById = function(req, res) {
  EcommerceMapping.findById(req.params.mappingId, function(err, task) {
    if (err)
      res.send(err);
    res.json(task);
  });
};


exports.update = function(req, res) {
  EcommerceMapping.findOneAndUpdate({_id: req.params.mappingId}, req.body, {new: true}, function(err, task) {
    if (err)
      res.send(err);
    res.json(task);
  });
};


exports.delete = function(req, res) {


  EcommerceMapping.remove({
    _id: req.params.mappingId
  }, function(err, task) {
    if (err)
      res.send(err);
    res.json({ message: 'EcommerceMapping successfully deleted' });
  });
};

